import styled from 'styled-components'
import { 
    CardIcon, 
    ImageBigCard, 
    ImageLittleLeftCard, 
    ImageLittleRightCard } from './Image'
import { Text } from './Text'

// Big Card
const CardComponent = styled.section`
    border: ${props => props.border};
    width: ${props => props.width};
    height: 200px;
    border-radius: 15px;
    display: grid;
    align-items: center;
    grid-auto-flow: column;
`
const CardSection = styled.section`
    justify-self: start;
    display: grid;
    align-items: center;
    justify-content: center;
    width: ${props => props.width};
    height: ${props => props.height};
`
const CardMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two Four Six'
                        'One Three Five Seven';
    grid-template-rows: repeat(3, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    padding-top: 20px;
    width: ${props => props.width};
    height: ${props => props.height};
`
const BigCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: #099CB0;
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 230px;
    height: 150px;
    padding: 10px;
    cursor: pointer;
`
const BigNormalCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-content: center;
    align-items: center;
    grid-auto-flow: column;
    column-gap: 10px;
    width: 187px;
    height: 70px;
    cursor: pointer;
`
export const BigCard = (props)=>{
    return(
        <CardComponent 
            width='fit-content'
            border='solid 4px #099CB0'>
            <CardSection width='328px' height='192px'>
                <ImageBigCard src={props.image}/> 
                <Text
                    title={props.bigCardTextImage}
                    fontFamily='Montserrat-Regular'
                    color='white'
                    fontSize='36px'/>
            </CardSection>
            <CardMenuSection width='880px' height='192px'>
                <BigCardIcon gridArea='One' onClick={props.bigCard.link}>
                    <CardIcon 
                        src={props.bigCard.icon}
                        width='70px'
                        height='70px'/>
                    <Text 
                        title={props.bigCard.title}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='25px'/>
                    <Text 
                        title={props.bigCard.subTitle}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='16px'/>
                </BigCardIcon>
                {props.bigNormalCards.map((bigNormaCard)=>(
                    <BigNormalCardIcon
                        key={bigNormaCard.id}
                        gridArea={bigNormaCard.gridArea}
                        backgroundColor='#099CB0'>
                        <Text 
                            title={bigNormaCard.title}
                            fontFamily='Montserrat-Regular'
                            color='white'
                            fontSize='22px'/>
                        <CardIcon
                            src={bigNormaCard.icon}
                            width='44px'
                            height='38px'/>
                    </BigNormalCardIcon>
                ))}
            </CardMenuSection>
        </CardComponent>
    )
}
// Little Cards
const CardComponentRight = styled.section`
    border: ${props => props.border};
    width: ${props => props.width};
    height: 200px;
    border-radius: 15px;
    display: grid;
    align-items: center;
    justify-content: end;
    grid-auto-flow: column;
    column-gap: 20px;
`
const CardContainerComponent = styled.div`
    display: inline-grid;
    grid-auto-flow: column;
    justify-content: center;
    align-items: center;
    column-gap: 20px;
    width: 100%;
`
const LittleImageCardSection = styled.section`
    justify-self: start;
    display: grid;
    align-items: center;
    justify-content: center;
    width: 172px;
    height: 192px;
`
const CardLeftMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two'
                        'One Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};
`
const CardRightMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Three'
                        'Two Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};
`
const NormlaCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 186px;
    height: 150px;
    padding: 10px;
    cursor: pointer;
`
export const LittleCard = (props)=>{
    return(
        <CardContainerComponent>
            <CardComponent
                width='602px'
                border='solid 4px #005F98'>
                <LittleImageCardSection>
                    <ImageLittleLeftCard src={props.leftImage}/> 
                    <Text
                        title={props.leftTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='36px'/>
                </LittleImageCardSection>
                <CardLeftMenuSection>
                    <NormlaCardIcon 
                        backgroundColor='#005F98'
                        gridArea='One' 
                        onClick={props.leftCard.link}>
                        <Text 
                            title={props.leftCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='22px'/>
                        <CardIcon 
                            src={props.leftCard.icon}
                            width='100px'
                            height='100px'/>
                    </NormlaCardIcon>
                    {props.leftNormalCards.map((leftNormalCard)=>(
                        <BigNormalCardIcon
                            key={leftNormalCard.id}
                            gridArea={leftNormalCard.gridArea}
                            backgroundColor='#005F98'>
                            <Text 
                                title={leftNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='22px'/>
                            <CardIcon
                                src={leftNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                </CardLeftMenuSection>
            </CardComponent>
            <CardComponentRight 
                width='602px'
                border='solid 4px #08A68E'>
                <CardRightMenuSection>
                    {props.rightNormalCards.map((rightNormalCard)=>(
                        <BigNormalCardIcon
                            key={rightNormalCard.id}
                            gridArea={rightNormalCard.gridArea}
                            backgroundColor='#08A68E'>
                            <Text 
                                title={rightNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='22px'/>
                            <CardIcon
                                src={rightNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                    <NormlaCardIcon 
                        backgroundColor='#08A68E'
                        gridArea='Three' 
                        onClick={props.rightCard.link}>
                        <CardIcon 
                            src={props.rightCard.icon}
                            width='150px'
                            height='40px'/>
                        <Text 
                            title={props.rightCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='14px'/>
                    </NormlaCardIcon>
                </CardRightMenuSection>
                <LittleImageCardSection width='602px' height='192px'>
                    <ImageLittleRightCard src={props.rightImage}/> 
                    <Text
                        title={props.rightTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='36px'/>
                </LittleImageCardSection>
            </CardComponentRight>
        </CardContainerComponent>
    )
}
