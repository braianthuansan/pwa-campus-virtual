import styled from 'styled-components'

export const ImageBigCard = styled.img`
    width: 328px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
`
export const ImageLittleLeftCard = styled.img`
    width: 172px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
`
export const ImageLittleRightCard = styled.img`
    width: 172px;
    height: 192px;
    align-self: start;
    justify-self: start;
    position: absolute;
    z-index: -1;
`
export const CardIcon = styled.img`
    width: ${props => props.width};
    height: ${props => props.height};
    object-fit: contain;
`