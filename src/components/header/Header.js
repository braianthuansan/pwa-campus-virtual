import React from 'react'

import { HeaderContainer } from '../container/HeaderContainer'
import { HeaderBackgroud, Logo } from './Image'
import { Text } from './Text'

export const Header = (props) => {
    return (
        <HeaderContainer>
            <HeaderBackgroud/>
            <Logo src={props.src}/>
            <Text>{props.text}</Text>
        </HeaderContainer>
    )
}